def clean_newline(str, replacement=""):
    strN = str.replace("\n", replacement)
    return strN


def address_tolower(str):
    return str.lower()


def remove_whitespace(str):
    return str.strip()


def str2int(str):
    if str.isnumeric():
        return int(str)
    else:
        return str


def clean_address(addr, addrType):
    position = 0

    # if addrType.upper() == "ROAD":
    addrSplit = addr.split(",")
    addrSplit = [ele.upper().strip(" ") for ele in addrSplit]
    # print("\n\n", addrSplit)

    # SHOP NO. 34, G/F., YAT TUNG SHOPPING CENTRE, YAT TUNG ESTATE, 8 YAT TUNG STREET, TUNG CHUNG, HONG KONG
    # addrSplit value : ["SHOP NO. 34", "G/F.", "YAT TUNG SHOPPING CENTRE", "YAT TUNG ESTATE", "8 YAT TUNG STREET", "TUNG CHUNG", "HONG KONG"]

    for ele in addrSplit:
        if addrType in ele:
            if re.search(r'\d', ele) is not None:
                addrSplit[position] = ele[re.search(r'\d', ele).start():]
                addrCleaned = ', '.join(addrSplit[position:])
                return addrCleaned
                break
            else:
                addrCleaned = ', '.join(addrSplit[position:])
                return addrCleaned
                break
        else:
            position += 1


def add_comma_behind_str(raw, keyword):
    if keyword in raw:
        str = raw.upper().split(keyword.upper())
        return (str[0] + keyword.upper() + ", " + str[1]).title()
    else:
        return raw
