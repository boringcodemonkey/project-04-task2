import requests
import json
#import datetime
import re
from bs4 import BeautifulSoup
from datetime import datetime
import zlib
import glob
import os
import shutil


def clean_facilities(sText):
    if sText == None:
        return ""
    else:
        soup = BeautifulSoup(sText, "html.parser")
        cText = soup.text
        return cText


def DMS_to_WGS84(DMS):
    DMSSplit = DMS.split("-")
    d = int(DMSSplit[0])
    m = int(DMSSplit[1])
    sd = int(DMSSplit[2])
    dec = 0.0
    if d >= 0:
        dec1 = d + float(m)/60 + float(sd)/3600
    else:
        dec1 = d - float(m)/60 - float(sd)/3600
    return dec1


def find_court_no(a):
    if a != None or a == "":
        returnValue = a.strip()[:2]
        if returnValue.isnumeric():
            return int(a)
        else:
            return "No information provided"
    else:
        return "No information provided"


def clean_phone_num(a):
    phoneNum = a  # 1234 5678 / 9378 4389
    if type(a) != int:
        phoneNum = a.replace(" ", "")
        if phoneNum.isnumeric():
            phoneNum = [int(phoneNum)]
        else:
            phoneNum = phoneNum.split("/")
            # print(phoneNum)
    else:
        phoneNum = [int(phone) for phone in phoneNum]
    # print(phoneNum)
    return phoneNum


def convert_opening_hours(a, b):
    aSplit = a.split(b)
    start = aSplit[0].strip().replace(" ", "")
    end = aSplit[1].strip().replace(" ", "")
    if ":" in start:
        inTime = datetime.strptime(start, "%I:%M%p")
        startTime = datetime.strftime(inTime, "%H:%M")
    else:
        inTime = datetime.strptime(start, "%I%p")
        startTime = datetime.strftime(inTime, "%H:%M")
    if ":" in end:
        inTime = datetime.strptime(end, "%I:%M%p")
        endTime = datetime.strftime(inTime, "%H:%M")
    else:
        inTime = datetime.strptime(end, "%I%p")
        endTime = datetime.strftime(inTime, "%H:%M")
    # return("{}-{}".format(startTime, endTime))
    return([startTime, endTime])


def find_opening_hours(a, b):
    if b == "blUhXe01yV":
        a = "5:30 am to 11:30 pm daily"
        #"7 am to 11 pm daily"
        #"7 am to 11 pm daily (1 hour per session)"

        #"24 hours daily"
        #"7am – 9pm daily"

        #"7:30 am &ndash; 10:00 pm daily"
        #"7:00 am &ndash; 11:00 pm daily (No provision of floodlight)"
    if "daily" in a:
        a = a[:a.find("daily")].strip()
    else:
        # print(a.find("("))
        a = a[:a.find("(")]
    a = a.replace("–", "-")
    # print()
    # print(b)
    # print(a)

    if a == "24 hours":
        openingHours = "00:00-23:59"
        # print(openingHours)
        return openingHours

    elif "to" in a:
        openingHours = convert_opening_hours(a, "to")
        # print(openingHours)
        return openingHours

    elif "ndash" in a:
        openingHours = convert_opening_hours(a, "&ndash;")
        # print(openingHours)
        return openingHours

    elif "-" in a:
        openingHours = convert_opening_hours(a, "-")
        # print(openingHours)
        return openingHours

    else:
        # print("Unknown")
        return "Unknown"


def determine_overall(a, b):
    if a != None:
        if type(b) == list:
            returnValue = False
            for keyword in b:
                if keyword.lower() in a.lower():
                    returnValue = True
                    break
            return returnValue

        else:
            if b.lower() in a.lower():
                return True
            else:
                return False
    else:
        return False


def determine_overall_with_remark(a, b):
    if a == True or b == True:
        return True
    else:
        return False
