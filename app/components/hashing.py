import requests
import numpy
import time
import datetime
import os
import json
import csv
import xmltodict
import zlib
import hashlib
from hashlib import md5, sha1


def crc32(raw):
    # print("crc32, raw data {}".format(raw))
    return hex(zlib.crc32(raw.encode("utf-8")))


def md5(raw):
    m = hashlib.md5()
    m.update(raw.encode("utf-8"))
    return m.hexdigest()


def generate_hash_value(a, b, c):
    '''
    a: Name_cn
    b: Opening_hours_cn
    c: Remarks_cn
    '''
    if c == None:
        c = ""
    s = a + b + c
    # print(s)
    t = crc32(s)

    return t
