import os
import glob
import json
from datetime import date, datetime
import re
from tkinter.tix import Tree
from numpy import ERR_LOG
import requests
from lxml import etree


from components.io import requests_from_url
from components.io import convert_XML2JSON
from components.io import write_file

from components.string_process import clean_newline
from components.string_process import remove_whitespace
from components.string_process import address_tolower
from components.string_process import str2int
from components.string_process import add_comma_behind_str

from components.hashing import crc32
from components.hashing import generate_hash_value

from components.helper import clean_facilities
from components.helper import DMS_to_WGS84
from components.helper import clean_phone_num
from components.helper import find_court_no
from components.helper import find_opening_hours
from components.helper import determine_overall
from components.helper import determine_overall_with_remark

headers = {"Content-Type": "application/json",
           "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:50.0) Gecko/20100101 Firefox/50.0",
           "Connection": "close"}


def download_files():
    # download the file into memory
    res = requests.get(
        "https://www.lcsd.gov.hk/datagovhk/facility/facility-hssp7.json", headers=headers)

    # write json file into the HDD
    myfile = open("../data/raw/json/soceer_pitches_data_raw{}{}.json".format("_",
                  date.today()), "w")
    jsonData = json.loads(res.text)
    jsonToWrite = json.dumps(
        jsonData, indent=4, sort_keys=True, ensure_ascii=False)
    # print(res.json())
    myfile.write(jsonToWrite)
    myfile.close()

    return res


def process_string(str):
    return remove_whitespace(clean_newline(str, " "))


def vehicle_supported_type(s):
    if s == None:
        return "General"
    elif s.lower() == "Tesla only".lower():
        return "Tesla"
    elif s.lower() == "BYD only".lower():
        return "BYD"
    else:
        return "General"


def check_public_permit(s):
    if s == None:
        return True
    elif "permit" in s:
        return False
    else:
        return True


def create_rangeList(start, end):
    rL = []

    for i in range(start, end + 1):
        rL.append(i)

    return rL


def turn_parking_slot_into_list(s):

    regexNumber = ".*?([0-9]+)[\n]*"  # only the number at the end
    regexPrefix = "(.*?)[0-9]+[\n]*"

    # s = "D042 - D052, D106 - D112"
    if s == None:
        return ["No information provided"]
    elif s.lower() == "Tesla only".lower() or s == "BYD only".lower() or "permit" in s or s == "":
        return ["No information provided"]
    else:
        parking_slot_list = []

        raw_comma_split = s.split(",")
        # ["D042 - D052", "D106 - D112", D16]

        for ele in raw_comma_split:
            ele = process_string(ele)

            isRange = False

            if "-" in ele:  # D042 - D052
                isRange = True

            if not isRange:
                parking_slot_list.append(ele)

            if isRange:
                ele_dash_split = ele.split("-")

                for obj in ele_dash_split:
                    obj = process_string(obj)

                # D042 is ele_dash_split[0]
                item0_number = re.findall(
                    regexNumber, ele_dash_split[0])[-1]  # 042
                item0_prefix = ele_dash_split[0][0:ele_dash_split[0].find(
                    item0_number)]  # D0

                # D052 is ele_dash_split[-1]
                item1_number = re.findall(
                    regexNumber, ele_dash_split[-1])[-1]  # 042

                item1_prefix = ele_dash_split[-1][0:
                                                  ele_dash_split[-1].find(
                                                      item0_number)
                                                  ]  # D0

                rangeList = create_rangeList(
                    int(item0_number), int(item1_number))
                # 42, 43, 44

                for parkingNo in rangeList:
                    parking_slot_list.append(item0_prefix + str(parkingNo))
                    # D042, D043, D044

    return parking_slot_list


def create_string_for_checksum(location, parkingNo):

    if location == None:
        location = "None"

    if parkingNo == None:
        parkingNo = "None"

    return location + "_" + parkingNo


def process_data(data):
    newDataList = []
    counter = 0

    data2process = data

    cleanedJSONFileName = "cleaned_soceer_pitches_data__raw{}{}.json".format("_",
                                                                             date.today())

    # print(data2process)

    for ele in data2process:

        cleanFacilityString = clean_facilities(ele["Ancillary_facilities_en"])

        newD = {
            "uuid": ele["GIHS"],
            "checkSum": generate_hash_value(ele["Name_cn"], ele["Opening_hours_cn"], ele["Remarks_cn"]),
            "location": {
                "name": {
                    "en": ele["Name_en"].strip(),
                    "zh": ele["Name_cn"].strip(),
                },
                "address": {
                    "en": ele["Address_en"].strip(),
                    "zh": ele["Address_cn"].strip(),
                },
                "district": {
                    "en": ele["District_en"].strip(),
                    "zh": ele["District_cn"].strip()
                },
                "gecode": {
                    "lat": DMS_to_WGS84(ele["Latitude"]),
                    "lng": DMS_to_WGS84(ele["Longitude"]),
                    "DMS": {
                        "lat": ele["Latitude"],
                        "lng": ele["Longitude"],
                    }
                }
            },
            "court_no": find_court_no(ele["Court_no_en"]),
            "phone": clean_phone_num(ele["Phone"]),
            "opening_hours": {
                "start": find_opening_hours(
                    clean_facilities(ele["Opening_hours_en"]), ele["GIHS"]
                )[0],
                "end": find_opening_hours(
                    clean_facilities(ele["Opening_hours_en"]), ele["GIHS"]
                )[1]
            },
            "multiPurposeCourt": determine_overall_with_remark(
                determine_overall(cleanFacilityString, "multi"),
                determine_overall(ele["Remarks_en"], "multi")),
            "facilities": {
                "accessibility": {
                    "brailleDirectoryMap": determine_overall(cleanFacilityString, "braille directory"),
                    "tactileGuidePath": determine_overall(cleanFacilityString, "tactile guide path"),
                    "toilet": determine_overall(cleanFacilityString, "Accessible Toilet"),
                },
                "basketballCourt": determine_overall_with_remark(
                    determine_overall(cleanFacilityString, "basketball"),
                    determine_overall(ele["Remarks_en"], "basketball"),
                ),
                "carpark": determine_overall_with_remark(
                    determine_overall(cleanFacilityString, [
                                      "car park", "carpark"]),
                    determine_overall(ele["Remarks_en"], [
                                      "car park", "carpark"]),
                ),
                "changingRoom": determine_overall_with_remark(
                    determine_overall(cleanFacilityString, "changing"),
                    determine_overall(ele["Remarks_en"], "changing"),
                ),
                "cyclingTrack": determine_overall_with_remark(
                    determine_overall(cleanFacilityString, "cycling"),
                    determine_overall(ele["Remarks_en"], "cycling"),
                ),
                "elderlyFitnessEquipment": determine_overall_with_remark(
                    determine_overall(cleanFacilityString, "elderly fitness"),
                    determine_overall(ele["Remarks_en"], "elderly fitness"),
                ),
                "fitnessStation": determine_overall_with_remark(
                    determine_overall(cleanFacilityString, "fitness"),
                    determine_overall(ele["Remarks_en"], "fitness"),
                ),
                "handball": determine_overall_with_remark(
                    determine_overall(cleanFacilityString, "handball"),
                    determine_overall(ele["Remarks_en"], "handball"),
                ),
                "joggingTrack": determine_overall_with_remark(
                    determine_overall(cleanFacilityString, [
                                      "running", "jogging"]),
                    determine_overall(ele["Remarks_en"], [
                                      "running", "jogging"]),
                ),
                "kiosk": determine_overall_with_remark(
                    determine_overall(cleanFacilityString, "kiosk"),
                    determine_overall(ele["Remarks_en"], "kiosk"),
                ),
                "locker": determine_overall_with_remark(
                    determine_overall(cleanFacilityString, "locker"),
                    determine_overall(ele["Remarks_en"], "locker"),
                ),
                "pebbleWalkingTrail": determine_overall_with_remark(
                    determine_overall(cleanFacilityString, "pebble"),
                    determine_overall(ele["Remarks_en"], "pebble"),
                ),
                "playground": determine_overall_with_remark(
                    determine_overall(cleanFacilityString, [
                                      "play ", "playground"]),
                    determine_overall(ele["Remarks_en"], [
                                      "play ", "playground"]),
                ),
                "raceCourse": determine_overall_with_remark(
                    determine_overall(cleanFacilityString, "race course"),
                    determine_overall(ele["Remarks_en"], "race course"),
                ),
                "rollerSkatingRink": determine_overall_with_remark(
                    determine_overall(cleanFacilityString, "roller skating"),
                    determine_overall(ele["Remarks_en"], "roller skating"),
                ),
                "soccerPitch": {
                    "artificialTurf": determine_overall_with_remark(
                        determine_overall(cleanFacilityString, ["artificial"]),
                        determine_overall(ele["Remarks_en"], ["artificial"]),
                    ),
                    "naturalTurf": determine_overall_with_remark(
                        determine_overall(cleanFacilityString, ["natural"]),
                        determine_overall(ele["Remarks_en"], ["natural"]),
                    ),
                },
                "spectatorStand": determine_overall_with_remark(
                    determine_overall(cleanFacilityString, "spectator"),
                    determine_overall(ele["Remarks_en"], "spectator"),
                ),
                "tennisCourt": determine_overall_with_remark(
                    determine_overall(cleanFacilityString, "tennis"),
                    determine_overall(ele["Remarks_en"], "tennis"),
                ),
                "toilet": determine_overall_with_remark(
                    determine_overall(cleanFacilityString, "toilet"),
                    determine_overall(ele["Remarks_en"], "toilet"),
                ),
                "volleyballCourt": determine_overall_with_remark(
                    determine_overall(cleanFacilityString, "volleyball"),
                    determine_overall(ele["Remarks_en"], "volleyball"),
                ),
            }
        }

        # print(newStationData)

        newDataList.append(newD)
        counter += 1

        print("Processing {} of {}".format(
            counter, len(data2process)), end="\r", flush=True)

        if counter % 20 == 0:
            x = open("../data/cleaned/{}".format(cleanedJSONFileName), "w")
            x.write(json.dumps(newDataList, indent=4,
                    sort_keys=True, ensure_ascii=False))
            x.close()

    x = open("../data/cleaned/{}".format(cleanedJSONFileName), "w")
    x.write(json.dumps(newDataList, indent=4,
                       sort_keys=True, ensure_ascii=False))
    x.close()

    print("\nraw data: {} cleaned data: {}".format(
        len(data2process), len(newDataList)
    )
    )


def main():
    startTime = datetime.now()

    currentDir = os.getcwd()
    JSONfolder = "data/raw/json"

    rawFileName = "soceer_pitches_data_raw{}{}.json".format("_",
                                                            date.today())

    fullJSONPath = os.path.join(os.path.join(
        currentDir, JSONfolder), rawFileName)

    print(fullJSONPath)

    listofFile = glob.glob(os.path.join("../data/raw/json", rawFileName))

    # print(listofFile)

    if len(listofFile) == 0:
        rawFileExist = False
    else:
        if rawFileName in listofFile[-1]:
            rawFileExist = True
        else:
            rawFileExist = False

    if not rawFileExist:
        print("Raw JSON file is not exist, download the file throught the API ...\n")
        download_files()
        # x = open("../data/raw/json/{}".format(rawFileName))
        # data = json.load(x)
        # print(data)
        # x.write(json.dumps(data, ensure_ascii=False, indent=4))
        # x.close()

    else:
        print("raw file exist, will read the file now")

    x = open("../data/raw/json/{}".format(rawFileName))
    data = json.load(x)

    print(data)

    process_data(data)

    timeNow = datetime.now()
    print(
        "Total use {} seconds to run".format(
            round((timeNow - startTime).total_seconds()))
    )


if __name__ == "__main__":
    main()
